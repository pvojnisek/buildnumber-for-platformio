# Build number generator for C and PlatformIO
This short script automatically increments a number (stored in 'versioning' file) before every build started from PlatformIO environment.

## Files:
- buildscript_versioning.py: the script to run automatically.
- versioning: auto generated file to store the last build number.
- include/version.h: auto generated file. The generated header to be included to your code.

## Installation to your project

1.  copy buildscript_versioning.py to your PIO project folder
2.  add to platformio.ini:

        extra_scripts = 
            pre:buildscript_versioning.py

3.  Build your project. New header file will be created: include/version.h
4.  Insert the necessary include line to your code:

        #include <version.h>

5.  Enjoy..
 
## Definitions for C
- BUILD_NUMBER
- VERSION: Currently the main version number is set in the buildscript_versioning.py script. If you need to increment the main version, you can do it here.
- VERSION_STR

## References
- basic idea of this project: [Stack Overflow](https://stackoverflow.com/questions/56923895/auto-increment-build-number-using-platformio)
- reference to start from: [Starting from this code](https://cplusadd.blogspot.com/2008/12/automatic-build-number-generation.html)
